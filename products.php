<?php require_once('header.php'); ?> 
            <div id="banner-wrapper" class="sub-page" style="background-image: url('assets/images/product-banner.jpg');"> <!-- begin banner-wrapper -->
                <div class="page-heading right wow bounceInRight" style="text-align: right; font-family: 'Raleway', sans-serif;">
                    <img src="assets/images/product-text.png" alt="">
                    <p style="font-size: 13px;"><span style="font-size: 24px;">AVAYA IP Office:</span> <br /> 
                    Simple, Powerful Communications for Small and <br />Medium Size Businesses</p>
                </div>
            </div> <!-- end banner-wrapper -->
			<div id="content-wrapper"> <!-- begin content-wrapper -->
                <div class="top-bar">
                    <h1>Our Products</h1>
                    <ul>
                        <li><a href="#">Home</a></li>
                        <li><a href="#">Our Products</a></li>
                    </ul>
                </div>
                <div class="content-inner">
                    <img src="assets/images/avaya.jpg" alt="" style="float: right; margin-top: 20px;" class="wow bounceInRight">
                    <p style="font-size: 30px; color: red; line-height: 24px;" class="wow bounceInLeft">AVAYA IP Office: <br />
                    <span style="font-size: 24px; color: red;">Simple, Powerful Communications for Small and Medium </span><br/>
                    <span style="font-size: 24px; color: red;">Size Businesses</span></p>
                    <a href="#"><img src="assets/images/button-download.png" alt="" class="wow flipInX"></a>
                    <p class="wow fadeInUp">Avaya IP Office is the right choice for any small or medium business, regardless of size and the number of locations. This flexible solution provides your employees the tools to handle all of their business communications using the device of their choice—laptop, mobile phone, office phone, home phone, or iPad—and the connection of their choice over IP, digital, analog, SIP, or wireless. It delivers unified communications in a single compact solution with leading-edge capabilities that help your employees work better and serve your customers more effectively and efficiently.</p>
                    <p class="wow fadeInUp">Successful companies are always looking for new ways to do more with less—keeping their business flexible, innovative, and competitive without driving up operating costs and capital expenses. The industry-recognized award-winning Avaya IP Office solution easily adapts to your goals and budget. It provides individual user productivity solutions to give each of your employees the capabilities they uniquely need, whether they are receptionists, home office workers, sales or service representatives, telecommuters, or mobile workers.</p>
                    <p class="wow fadeInUp"><strong>Getting all of your corporate communications working together is a key element in driving better business performance. Effective communications—whether by office phone, cell phone, email, text, or instant messaging—are essential for positive customer interactions. Has any of the following happened in your company?</strong></p>
                    <div class="col-2 wow bounceInLeft">
                        <ul>
                            <li><p>A customer or prospect calls with an urgent, time-sensitive request, but is unable to reach the right person for an answer.</p></li>
                            <li><p>An unexpected development—severe weather, a personal matter—leaves employees unable to come into the office and completely unproductive.</p></li>
                        </ul>
                    </div>
                    <div class="col-2 wow bounceInRight">
                        <ul>
                            <li><p>An unexpected development—severe weather, a personal matter—leaves employees unable to come into the office and completely unproductive.</p></li>
                            <li><p>You suspect peak call volumes result in lost calls, but have no idea how many or how much it’s costing your business.</p></li>
                        </ul>
                    </div>
                    <p class="wow fadeInUp">These are all everyday occurrences that can impact your image, your employees’ ability to perform at their best, your customer service interactions, and ultimately your bottom line. Avaya IP Office is ready to address each of those situations while offering additional benefits.</p>
                    <p class="wow fadeInUp">Unlike other vendors' solutions, Avaya IP Office uses built-in intelligence to simplify your company's use of wired, wireless, and Internet communications. Instead of keeping them separate, IP Office brings them together so you can easily turn a home or mobile phone into an office extension, collaborate with dozens of customers or staff members on a conference call, and get detailed reports that show how well your company is responding to customers. With such rich functionality, IP Office naturally creates new ways of thinking about the role unified communications can play in your business.</p>
                    <p class="tip wow bounceInLeft"><img src="assets/images/wireless-icon.png" alt="">Rely on IP Office to Control Communication Costs</p>
                    <ul class="wow bounceInRight">
                        <li><p>IP Office is highly effective at helping your business control communication costs:
                        Choose from a wide range of options to better manage your company’s mobile phone and long-distance charges by routing calls through the system and over broadband links.</p></li>
                        <li><p>Handle more calls with fewer people by using the built-in intelligence in IP Office to route calls quickly and accurately.</p></li>
                        <li><p>Expand without adding office space by having chosen employees work from home or alternate locations with no loss of functionality as each continues to have access to all the communications and call handling capabilities as your in-office employees.</p></li>
                        <li><p>Eliminate conference calling fees by using the two built-in 64-party conference bridges.</p></li>
                    </ul>
                    <p class="tip wow bounceInLeft"><img src="assets/images/network-icon.png" alt="">Connect Multiple Office Locations</p>
                    <p class="wow bounceInRight">IP Office is ideal for any small to mid-size company with multiple locations as up to 32 IP Office sites can connect to communicate seamlessly. This not only eliminates site-to- site calling costs, but saves by sharing messaging, receptionists, and the office directory. All connected systems can also be managed from a single browser-based interface. For continuous operation, users with IP phones can automatically failover to another location, retaining full communications capabilities in the event of a power outage.</p>
                    <p class="tip wow bounceInLeft"><img src="assets/images/computer-icon.png" alt="">Easy to Manage</p>
                    <p class="wow bounceInLeft">Whether you're adding a new employee or a new office, setting up a customer service help desk, or outfitting an employee to work at home, IP Office keeps it simple.</p>
                    <ul class="wow bounceInRight">
                        <li><p>Swiftly deploy customized features or take advantage of pre-existing templates to quickly deploy those features to all employees or only to certain individuals.</p></li>
                        <li><p>Seamlessly integrate a wide range of business applications from Avaya and Avaya partners to enhance and customize your IP Office system to the specific needs of your business.</p></li>
                        <li><p>Adding new phones or other devices is a matter of plugging them in and powering on as IP Office automatically reads the IP address of the device. Once set up, a device can be moved from extension to extension, even to a location such as a home office, without having to re-administer the system.</p></li>
                        <li><p>Securely manage the system from anywhere and at any time. Centralize remote management and administration for businesses with more than one location, eliminating the need for an administrator at each site.</p></li>
                    </ul>
                    <p class="tip wow bounceInLeft"><img src="assets/images/light-icon.png" alt="">A Solution You Can Depend On</p>
                    <p class="wow bounceInRight">Everything included in an IP Office solution is designed to keep your communications at peak performance at all times: Due to advanced remote diagnostics capabilities and built-in resiliency, IP Office proactively identifies potential problems before they can cause an outage or business disruption. The system can be configured to notify the administrator of a system problem in a variety of ways, including email notification.</p>
                    <p class="tip wow bounceInLeft"><img src="assets/images/group-icon.png" alt="">Get the Power of Unified Communications from Us</p>
                    <p class="wow bounceInRight">Avaya, a global leader in business communications systems, and our experienced certified Avaya Channel Partners understand the challenges facing small and growing businesses. Let us work with you to create an Avaya IP Office solution that fits your business’s unique challenges, capabilities, and needs.</p>
                    <p class="tip wow bounceInLeft">With <img src="assets/images/sm-avaya.jpg" alt="">IP Office, you will.</p>
                    <p class="wow bounceInRight">Communicate like never before. Respond immediately. Share information, anytime, anywhere, via any device. You’ll have the power of a unified communications system that connects everyone—your people, your customers, your partners. A system that’s incredibly sophisticated, yet remarkably simple to use. Go ahead and grow—IP Office is fully capable of handling up to 2,500 users in a single site or across multiple sites. With Avaya, you have a complete, across-the-board solution that brings it all together. From telephony and video to mobility and call center applications, to networking, security, and ongoing services, Avaya IP Office will help give your business a competitive edge. Let you do more, with less. Drive profitable growth, without driving up costs. Perform better now and in the future.</p>
                </div>
            </div> <!-- end content-wrapper -->

<?php require_once('footer.php'); ?> 