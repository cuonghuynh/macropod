<?php require_once('header.php'); ?> 

            <div id="banner-wrapper" class="sub-page" style="background-image: url('assets/images/product-banner.jpg');"> <!-- begin banner-wrapper -->
                <div class="page-heading right wow bounceInRight" style="text-align: right; font-family: 'Raleway', sans-serif;">
                    <img src="assets/images/product-text.png" alt="">
                    <p style="font-size: 13px;"><span style="font-size: 24px;">AVAYA IP Office:</span> <br /> 
                    Simple, Powerful Communications for Small and <br />Medium Size Businesses</p>
                </div>
            </div> <!-- end banner-wrapper -->
			<div id="content-wrapper"> <!-- begin content-wrapper -->
                <div class="top-bar">
                    <h1>Our Services</h1>
                    <ul>
                        <li><a href="/">Home</a></li>
                        <li><a href="#">Our Services</a></li>
                    </ul>
                </div>
                <div class="content-inner">
                    <p class="wow fadeInUp">MACROPOD TECHNOLOGY specializes in providing comprehensive technical supports and customer services on a personal level to companies seeking increased professionalism and reliability. We also supply quality state-of-the-art ip phone system& unified communications, computing hardware, peripherals, and software specific to your needs and requirements.</p>
                    <div class="col-wrap">
                        <div class="col-2  wow fadeInUp">
                            <div class="title">
                                <img src="assets/images/wireless-icon.png" alt="">
                                <p class="tip">IP PHONE & UNIFIED <br/>COMMUNICATIONS SYSTEM</p>
                            </div>
                            <ul>
                                <li>IP Phone Communications System with customize needs from customers</li>
                                <li>Contact Centers Solutions for Customer Service & Business Sales Team</li>
                                <li>Video Conferencing Solutions</li>
                            </ul>
                        </div>
                        <div class="col-2  wow fadeInUp">
                            <div class="title">
                                <img src="assets/images/computer-icon.png" alt="">
                                <p class="tip">PERSONAL COMPUTING SYSTEMS</p>
                            </div>
                            <ul>
                                <li>Supply of PCs, Notebook, Workstations & Thin Clients</li>
                                <li>Rental of PC, Notebook & Projector</li>
                                <li>Leasing of PCs & Notebook</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-wrap">
                        <div class="col-2  wow fadeInUp">
                            <div class="title">
                                <img src="assets/images/building-icon.png" alt="">
                                <p class="tip">ENTERPRISE SYSTEMS</p>
                            </div>
                            <ul>
                                <li>Supply, Commissioning & Maintenance support of Data Centre Equipments and Systems Raised Floor, Precision Air-conditioning & Water Detection System, Centralized UPS, Fire Suppression Systems Castle Door Access System, CCTV Security System and Equipment Racking System</li>
                                <li>Enterprise & Application Servers / Virtualization</li>
                                <li>Back-up Systems / Media & Disaster Recovery Solutions</li>
                                <li>Application, Firewall Security & Management Software and Licensing</li>
                            </ul>
                        </div>
                        <div class="col-2  wow fadeInUp">
                            <div class="title">
                                <img src="assets/images/network-icon.png" alt="">
                                <p class="tip">NETWORK INTEGRATION SYSTEMS</p>
                            </div>
                            <ul>
                                <li>Local Area Network Design & Implementation.</li>
                                <li>Structured UTP Cabling & Fiber Optics Cabling System.</li>
                                <li>Local Area Network Security System & Load Balancer ( Appliances & Solutions ).</li>
                                <li>Networking Hardware & Supplies</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-wrap">
                        <div class="col-2  wow fadeInUp">
                            <div class="title">
                                <img src="assets/images/general-icon.png" alt="">
                                <p class="tip">TECHNICAL SUPPORTS & SERVICES PROGRAM</p>
                            </div>
                            <ul>
                                <li>ICT Installation & Technical Services</li>
                                <li>IT Outsourcing & Maintenance Contract</li>
                            </ul>
                        </div>
                        <div class="col-2  wow fadeInUp">
                            <div class="title">
                                <img src="assets/images/support-icon.png" alt="">
                                <p class="tip">PRIVATE CLOUD SOLUTIONS</p>
                            </div>
                            <p>Private Cloud Storage for your company, is a secure private cloud storage solution for the enterprise.
It provides comprehensive administrative control to reinforce data security & Accelerate your workflow with a best-of-breed file-sharing solution.</p>
                            <em>Private Cloud Storage</em>
                            <ul>
                                <li>All data is saved securely under your control </li>
                                <li>Support instant, secure access to all your data </li>
                                <li>Secure file sharing from any devices on the go</li>
                            </ul>
                            <em>Complete Online Workspace</em>
                            <ul>
                                <li>Use File and Folder Link to share large files and folders easily</li>
                                <li>Set up Shared Folder for secure internal collaboration</li>
                                <li>Add a Guest Folder for efficient collaboration with partners</li>
                            </ul>
                            <em>PC Remote Access </em>
                            <ul>
                                <li>Supports remote access to your PC from the Mobile App</li>
                                <li>Install PC agent to enable remote access</li>
                            </ul>
                            <em>Secure & Exclusive Administrative Control </em>
                            <ul>
                                <li>It provides exclusive administrative control</li>
                                <li>to manage your company's data securely</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div> <!-- end content-wrapper -->

<?php require_once('footer.php'); ?> 