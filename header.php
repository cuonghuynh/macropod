<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->

        <link href='http://fonts.googleapis.com/css?family=Cabin:400,700' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Raleway:400,700' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,700' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="assets/css/normalize.css">
        <link rel="stylesheet" href="assets/css/animate.css">
        <link rel="stylesheet" href="style.css">
        <script src="assets/js/vendor/modernizr-2.8.3.min.js"></script>

    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <div id="site-wrapper"> <!-- begin site-wrapper -->
            <header> <!-- begin header -->
                <div class="logo-wrapper">
                    <a href="/" class="logo" ><img src="assets/images/logo.png" alt=""></a>
                    <div class="search-form">
                        <form action="">
                            <input type="search" name="search">
                            <input type="submit" value="Search">
                        </form>
                    </div>
                </div>
                <nav>
                    <ul class="main-nav">
                        <?php

                            $current = $_SERVER['REQUEST_URI']; 

                            $parts = explode('/', $current);

                            $current = $parts[1];

                         ?>
                        <li><a href="/" class="<?php if (empty($current)) { echo 'active'; } ?>">Home</a></li>
                        <li><a href="about-us" class="<?php if ($current == 'about-us') { echo 'active'; } ?>">About Us</a></li>
                        <li><a href="products" class="<?php if ($current == 'products') { echo 'active'; } ?>">Our Products</a></li>
                        <li><a href="services" class="<?php if ($current == 'services') { echo 'active'; } ?>">Our Services</a></li>
                        <li><a href="business" class="<?php if ($current == 'business') { echo 'active'; } ?>">Our Partners</a></li>
                        <li><a href="client" class="<?php if ($current == 'client') { echo 'active'; } ?>">Client</a></li>
                        <li><a href="contact-us" class="<?php if ($current == 'contact-us') { echo 'active'; } ?>">Contact Us</a></li>
                    </ul>
                </nav>
            </header> <!-- end header -->
             