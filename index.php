<?php require_once('header.php'); ?> 
            <div id="banner-wrapper" style="background-image: url('assets/images/home-banner.jpg');"> <!-- begin banner-wrapper -->
                <div class="page-heading right wow bounceInRight">
                    <img src="assets/images/macropod-text.png" alt="">
                    <p>Committed in providing state-of-the-art ICT products and comprehensive supports focusing on integrating prevailing and emerging information technologies that will bring greater value to our customer needs and business growth</p>
                </div>
            </div> <!-- end banner-wrapper -->
            <div id="content-wrapper"> <!-- begin content-wrapper -->
                <div class="box wow fadeInUp">
                    <div class="box-inner">
                        <div class="title">
                            <img src="assets/images/wireless-icon.png" alt="">
                            <p>IP PHONE & UNIFIED COMMUNICATIONS SYSTEM</p>
                        </div>
                        <div class="description">
                            <ul>
                                <li>IP Phone Communications System with customize needs from customers</li>
                                <li>Contact Centers Solutions for Customer Service & </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="box wow fadeInUp">
                    <div class="box-inner">
                        <div class="title">
                            <img src="assets/images/computer-icon.png" alt="">
                            <p>PERSONAL COMPUTING SYSTEMS</p>
                        </div>
                        <div class="description">
                            <ul>
                                <li>Supply of PCs, Notebook, Workstations & Thin</li>
                                <li>Rental of PC, Notebook & Projector</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="box last wow fadeInUp">
                    <div class="box-inner">
                        <div class="title">
                            <img src="assets/images/building-icon.png" alt="">
                            <p>ENTERPRISE SYSTEMS</p>
                        </div>
                        <div class="description">
                            <p>Supply, Commissioning & Maintenance support of Data Centre Equipments and Systems Raised Floor,
                            Precision Air-conditioning & Water Detection</p>
                        </div>
                    </div>
                </div>
                <div class="box wow fadeInUp">
                    <div class="box-inner">
                        <div class="title">
                            <img src="assets/images/network-icon.png" alt="">
                            <p>NETWORK INTEGRATION SYSTEMS</p>
                        </div>
                        <div class="description">
                            <ul>
                                <li>Local Area Network Design & Implementation.</li>
                                <li>Structured UTP Cabling & Fiber Optics Cabling System.</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="box wow fadeInUp">
                    <div class="box-inner">
                        <div class="title">
                            <img src="assets/images/general-icon.png" alt="">
                            <p>TECHNICAL SUPPORTS & SERVICES PROGRAM</p>
                        </div>
                        <div class="description">
                            <ul>
                                <li>ICT Installation & Technical Services</li>
                                <li>IT Outsourcing & Maintenance Contract...</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="box last wow fadeInUp">
                    <div class="box-inner">
                        <div class="title">
                            <img src="assets/images/support-icon.png" alt="">
                            <p>PRIVATE CLOUD SOLUTIONS</p>
                        </div>
                        <div class="description">
                            <p>Private Cloud Storage for your company, is a secure private cloud storage solution for the enterprise.</p>
                        </div>
                    </div>
                </div>
            </div> <!-- end content-wrapper -->

<?php require_once('footer.php'); ?> 