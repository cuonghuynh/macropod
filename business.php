<?php require_once('header.php'); ?> 

            <div id="banner-wrapper" class="sub-page" style="background-image: url('assets/images/business-banner.jpg');"> <!-- begin banner-wrapper -->
                <div class="page-heading right  wow bounceInRight" style="text-align: right; font-family: 'Raleway', sans-serif; color: #000;">
                    <img src="assets/images/business-text.png" alt="">
                    <p>We also supply quality state-of-the-art ip phone system & <br /> 
                        unified communications, computing hardware, <br />
                        peripherals, and software specific to your needs and <br />
                        requirements.</p>
                </div>
            </div> <!-- end banner-wrapper -->
			<div id="content-wrapper"> <!-- begin content-wrapper -->
                <div class="top-bar">
                    <h1>Our Partners</h1>
                    <ul>
                        <li><a href="/">Home</a></li>
                        <li><a href="#">Our Partners</a></li>
                    </ul>
                </div>
                <div class="content-inner">
                    <p class="wow fadeInUp">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris </p>
                    <h3 class="tip">SOFTWARE</h3>
                    <hr>
                    <table class="wow bounceInLeft">
                        <tr>
                            <td><img src="assets/images/business-partner/logo-1.jpg" alt=""></td>
                            <td><img src="assets/images/business-partner/logo-2.jpg" alt=""></td>
                            <td><img src="assets/images/business-partner/logo-3.jpg" alt=""></td>
                            <td><img src="assets/images/business-partner/logo-4.jpg" alt=""></td>
                        </tr>
                    </table>

                    <h3 class="tip">HARDWARE</h3>
                    <hr>
                    <table style="height: 445px;" class="wow bounceInRight">
                        <tr>
                            <td><img src="assets/images/business-partner/logo-5.jpg" alt=""></td>
                            <td><img src="assets/images/business-partner/logo-6.jpg" alt=""></td>
                            <td><img src="assets/images/business-partner/logo-7.jpg" alt=""></td>
                            <td><img src="assets/images/business-partner/logo-8.jpg" alt=""></td>
                        </tr>
                        <tr>
                            <td><img src="assets/images/business-partner/logo-9.jpg" alt=""></td>
                            <td><img src="assets/images/business-partner/logo-10.jpg" alt=""></td>
                            <td><img src="assets/images/business-partner/logo-11.jpg" alt=""></td>
                            <td><img src="assets/images/business-partner/logo-12.jpg" alt=""></td>
                        </tr>
                        <tr>
                            <td><img src="assets/images/business-partner/logo-13.jpg" alt=""></td>
                            <td><img src="assets/images/business-partner/logo-14.jpg" alt=""></td>
                            <td><img src="assets/images/business-partner/logo-15.jpg" alt=""></td>
                            <td><img src="assets/images/business-partner/logo-16.jpg" alt=""></td>
                        </tr>
                        <tr>
                            <td><img src="assets/images/business-partner/logo-17.png" alt=""></td>
                            <td><img src="assets/images/business-partner/logo-18.jpg" alt=""></td>
                            <td><img src="assets/images/business-partner/logo-19.png" alt=""></td>
                            <td><img src="assets/images/business-partner/logo-20.png" alt=""></td>
                        </tr>
                    </table>
                </div>
            </div> <!-- end content-wrapper -->

<?php require_once('footer.php'); ?> 