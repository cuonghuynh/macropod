<?php require_once('header.php'); ?> 

            <div id="banner-wrapper" class="sub-page" style="background-image: url('assets/images/contactus-banner.jpg');"> <!-- begin banner-wrapper -->
                <div class="page-heading right wow bounceInRight" style="text-align: right; font-family: 'Raleway', sans-serif; color: #000;">
                    <img src="assets/images/contact-us-text.png" alt="">
                    <p>We’d love to hear from you</p>
                </div>
            </div> <!-- end banner-wrapper -->
			<div id="content-wrapper"> <!-- begin content-wrapper -->
                <div class="top-bar">
                    <h1>Contact Us</h1>
                    <ul>
                        <li><a href="/">Home</a></li>
                        <li><a href="#">Contact Us</a></li>
                    </ul>
                </div>
                <div class="content-inner">
                    <div class="col-wrap wow fadeInUp">
                        <div class="col-2">
                            <p><b>MACROPOD TECHNOLOGY (M) SDN BHD 1068984 - A</b><br>
                            NO. 9, JALAN BK 1/19, KINRARA INDUSTRIAL PARK, <br>
                            47100 PUCHONG, SELANGOR DE. <br>
                            Tel :  603-8070 0079 <br/>  
                            Fax :  603-8079 0392</p>
                        </div>
                        <div class="col-2">
                            <p><strong>Business Hours</strong></p>
                            <p>MONDAY TO FRIDAY : 09.00AM TO 06.00PM <br>
                            SATURDAY & SUNDAY : BY APPOINTMENT</p>
                        </div>
                    </div>
                    <form action="" id="contact-form" class="wow fadeInUp" >
                        <table>
                            <tr>
                                <td>
                                    <label for="name"><span class="required">*</span>Name:</label><br>
                                    <input type="text" name="name" id="name" value="" required>  
                                </td>
                                <td>
                                    <label for="">Company Name:</label><br>
                                    <input type="text" name="company" id="company" value="">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="address">Address:</label><br>
                                    <input type="address" name="address" id="address" value="">  
                                </td>
                                <td>
                                    <label for="tel">Tel:</label><br>
                                    <input type="text" name="tel" id="tel" value="" >
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="email"><span class="required">*</span>Email:</label><br>
                                    <input type="email" name="email" id="email" value="" required>  
                                </td>
                                <td>
                                    
                                </td>
                            </tr>
                        </table>
                        <p>
                        <label for="message"><span class="required">*</span>Your Message</label><br>
                        <textarea name="message" id="message" required></textarea></p>
                        <img src="assets/images/ajax-loader.gif" alt="" class="loading">
                        <div class="submit-button">
                            <span>Send Message</span>
                            <input type="submit" id="submit" name="submit">
                        </div>
                        
                    </form>
                </div>
            </div> <!-- end content-wrapper -->

<?php require_once('footer.php'); ?> 