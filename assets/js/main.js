$(document).ready(function() {
	new WOW().init();

	$('#contact-form').submit(function(event) {
		event.preventDefault();

		$('.loading').css('display', 'initial');
		$('#submit').prop('disabled', true).css('cursor', 'not-allowed');

		var dt = new FormData(this);

		$.ajax({
			url: 'api_sendmail.php',
			type: 'POST',
			dataType: 'json',
			contentType: false,
   			processData: false,
			data: dt,
		})
		.done(function(res) {
			console.log(res);
			if (res == 'true') {
				alert('Enquiry email sent successfully');
				location.reload();
			} else {

				if (res['name']) {
					$('#name').addClass('fail animated shake');
				}

				if (res['email']) {
					$('#email').addClass('fail animated shake');
				}

				if (res['message']) {
					$('#message').addClass('fail animated shake');
				}
			}
			$('.loading').css('display', 'none');
			$('#submit').prop('disabled', false).css('cursor', 'pointer');
		})
		.fail(function(jqXHR, textStatus, errorThrown) {
			alert("The following error occurred: "+ textStatus + " " + errorThrown);
		});
		

	});
});