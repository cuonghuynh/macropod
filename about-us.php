<?php require_once('header.php'); ?> 
            <div id="banner-wrapper" class="sub-page" style="background-image: url('assets/images/about-us-banner.jpg');"> <!-- begin banner-wrapper -->
                <div class="page-heading left wow bounceInLeft">
                    <img src="assets/images/about-us-text.png" alt="">
                    <p>Committed in providing state-of-the-art ICT products and comprehensive supports focusing on integrating prevailing and emerging information technologies.</p>
                </div>
            </div> <!-- end banner-wrapper -->
			<div id="content-wrapper"> <!-- begin content-wrapper -->
                <div class="col-3 wow fadeInUp">
                    <div class="col-inner">
                        <h2>About <br/> MACROPOD TECHNOLOGY</h2>
                        <p>We are committed to supply the quality IT hardware and complete solutions that include systems and storage, networking and security, virtualization, unified communication and managed services. Our expertise in technology lifecycle management and best practices fulfilment methodology are core competencies that add value which compounds over time, helping our clients to reduce total cost of IT ownership.</p>
                    </div>
                </div>
                <div class="col-3 wow fadeInUp">
                    <div class="col-inner">
                        <br/><h2>CORPORATE VISION</h2>
                        <p>Our vision is to create long-term business partnership, guided by our values of aligning people and processes, to facilitate our solutions to deliver IT services with commitment, and we believe our long-term results show how much we care about building healthy and mutually beneficial partnerships.</p>
                    </div>
                </div>
                <div class="col-3 wow fadeInUp">
                    <div class="col-inner">
                        <br/><h2>CORPORATE VALUES</h2>
                        <p>We are committed to bring tangible IT services to our customers with the best value for their money, keeping their business flexible, innovative and competitive without driving up operating costs and capital expenses.</p>
                    </div>
                </div>
            </div> <!-- end content-wrapper -->

<?php require_once('footer.php'); ?> 