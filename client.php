<?php require_once('header.php'); ?> 

			<div id="banner-wrapper" class="sub-page" style="background-image: url('assets/images/business-banner.jpg');"> <!-- begin banner-wrapper -->
                <div class="page-heading right  wow bounceInRight" style="text-align: right; font-family: 'Raleway', sans-serif; color: #000;">
                    <img src="assets/images/client-text.png" alt="">
                    <p>We also supply quality state-of-the-art ip phone system & <br /> 
                        unified communications, computing hardware, peripherals, <br />
                        and software specific to your needs and requirements.</p>
                </div>
            </div> <!-- end banner-wrapper -->
			<div id="content-wrapper"> <!-- begin content-wrapper -->
                <div class="top-bar">
                    <h1>Client</h1>
                    <ul>
                        <li><a href="/">Home</a></li>
                        <li><a href="#">Client</a></li>
                    </ul>
                </div>
                <div class="content-inner">
                    <h2 class="tip wow bounceInLeft">Major Projects in Malaysia</h2>
                    <p class="wow bounceInRight">Macropod Technology major projects in Malaysia include:</p>
                    <table class="client wow fadeInUp">
                    	<tr>
                    		<th>Project</th>
                    		<th>Industry</th>
                    	</tr>
                    	<tr>
                    		<td>IP Phone - Unified Communications</td>
                    		<td>Retails</td>
                    	</tr>
                    	<tr>
                    		<td>IP Phone – Call Center Solutions</td>
                    		<td>Corporate / Retails</td>
                    	</tr>
                    	<tr>
                    		<td>AD Migration</td>
                    		<td>Insurance</td>
                    	</tr>
                    	<tr>
                    		<td>New Infrastructure</td>
                    		<td>Automobile</td>
                    	</tr>
                    	<tr>
                    		<td>Disaster Recovery</td>
                    		<td>Insurance</td>
                    	</tr>
                    	<tr>
                    		<td>Firewall and Internet Load Balancing</td>
                    		<td>Airline Corporate</td>
                    	</tr>

                    	<tr>
                    		<td>Server Consolidation</td>
                    		<td>Corporate</td>
                    	</tr>
                    	<tr>
                    		<td>Data Center – Design & Build</td>
                    		<td>Investor Services</td>
                    	</tr>
                    	<tr>
                    		<td>AD Implementation</td>
                    		<td>Manufacturer</td>
                    	</tr>
                    	<tr>
                    		<td>Cisco Switch</td>
                    		<td>Investment Holding Company</td>
                    	</tr>
                    </table>
                </div>
            </div> <!-- end content-wrapper -->

<?php require_once('footer.php'); ?> 