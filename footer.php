            <footer> <!-- begin footer -->
                <div class="footer-inner">
                    <p class="copyright">COPYRIGHT © 2015 MACROPOD TECHNOLOGY (M) SDN BHD. ALL RIGHTS RESERVED. </p>
                    <ul class="footer-nav">
                        <li><a href="#">Home</a></li>
                        <li><a href="#">About Us</a></li>
                        <li><a href="#">Our Products</a></li>
                        <li><a href="#">Our Services</a></li>
                        <li><a href="#">Our Partners</a></li>
                        <li><a href="#">Contact Us</a></li>
                    </ul>
                </div>
            </footer> <!-- end footer -->
        </div> <!-- end site-wrapper -->


        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script src="assets/js/plugins.js"></script>
        <script src="assets/js/wow.min.js"></script>
        <script src="assets/js/main.js"></script>

    </body>
</html>