<?php

require 'api/sendmail.php';

if (is_ajax()) {

	$errors = array();
	$oldInput = array();

	if (!empty($_POST)) {

		//validate form
		$name = $_POST['name'];
		$email = $_POST['email'];
		$company = $_POST['company'];
		$address = $_POST['address'];
		$tel = $_POST['tel'];
		$message = $_POST['message'];

		if (empty($name)) {
			$errors['name'] = 1;
		} else {
			$oldInput['name'] = $name;
		}

		if (empty($email)) {
			$errors['email'] = 1;
		} else {
			$oldInput['email'] = $email;
		}

		if (empty($message)) {
			$errors['message'] = 1;
		} else {
			$oldInput['message'] = $message;
		}

		if (count($errors) == 0) {

			$mail = new Mail(587,
				'smtp.gmail.com',
				'tls',
				'macropod.webmaster@gmail.com',
				'doporcam',
				'Macropod Technology Enquiry Form',
				'frederic@macropod.com.my',
				'Macropod Technology');

			$mail->addAddress('sales@macropod.com.my', 'Macropod Technology');

			if (!$mail->send('Contact Form', $name, $email, $company, $tel, $message, $address)) {
				echo json_encode($mail->errors);
			} else {
				echo json_encode('true');
			}

		} else {
			echo json_encode($errors);
		}

	}

}

//Function to check if the request is an AJAX request
function is_ajax() {
	return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
}

?>